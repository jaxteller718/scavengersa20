# ScavengersA20

When first staring your game you will receive a red null. THIS IS NORMAL.  The error is actually Walker Sim generating a config  file. You can play as normal and this null will not appear on subsequent startups.


Added Table Saw recipes and Parts to Harvest

Wood Frames now take Planks and Wood Nails to Craft in Table Saw

Added Ragsys Vehicle Madness Overhaul which spawns broken down vehicles in the world

Added Hinges to Door Harvests for Doors and Storage Crafts

Removed Steel and Military Armor Crafting

Moved Furniture and All Non Wood Frame/Shape recipes to Workbench

Moved Wood Recipes to Table Saw

Reduced pry time from boards

Zombies do less damage to dirt, metal and stone, more damage to wood

Zombies should now run properly on Nightmare speed

Removed All Custom POIs due to issues from existing RH blocks. With A20 changing POIs I will leave these out for now until we fix for 20

Added Tanning Rack

Added Leather Strips and replaced Leather harvests with them

Added Bone Fragments and replaced Bone harvests with them

Reduced Meat Harvests from Animals

Added Animal Hide and replaced Leather from animals with it

Removed Advanced Engineering Perk

Removed Grease Monkey Perk

Rearranged Basic Questline and Added Steps

Moved Repositories to Azure from Git

Wood Logs now yield wood on harvest instead of destroy

Lowered Headshot Multiplier

Lowered Scrap Iron Harvests on stations

Fixed Improper Backpack Unlocks

Added craftable Cardboard Box

Removed Perk Daring Adventurer

Removed Perk Master Chef

Removed Perk Better Barter

Added Fiber Wraps To use fists with

Added Damage to punching with bare hands

Added Menu Images 

Added Scomar Hud and UI 

Added Relix Menu Options Modlets 

Removed Walker Sim Messing Servers Up 

Removed Mutated Zombie 

Fixed Improper Vanilla Cop Model 

Increased Sight Range on Zombies 

Doubled Zombie Spawns 

Increased Zombie Respawn Days to Twice a Day 

Added Wood Nails to Frame Shapes 

Removed Frame Shapes from table saw 

Fixed Nulls on Cigar 

Fixed improper recipe craft tag on Leather 

Increased Grass, Bird Nest and Rock spawns in biomes

Removed all Mods Crafting 

Added 3 new backpack sizes 

Trader POIs now Vulnerable to Attack 

Removed Traders from Trader POIS 

Added Working Microwave with 50 percent chance of giveback 

Added bird nest destroys on loot as well as trash bags 

Added New Opening Quest Step 

Added Loot Container Code that Disallows Placing Items in POI Boxes 

Added Quality and Degradation 

Added Infinite recipe ingredients code 

Removed ability to pick up vehicles during Blood Moon 

Remade All Unity Assets in 2020 with Linear Lighting 

Added new headshot modlet.

Removed ability to buy perks

Added Perk Books. Reading one you find gives you one level in that perk.

Removed all recipe unlocks from perks

Updated Spherecore

Updated Take and Replace

Added Active Ingredients by Redbeard

Fixed and Readded Quality Degradation on Repair

Removed Random Walks

Enabled New Headshot Only Modlet

Starter Crate Changed Up to give uncleaned canteen, higher tier fist wraps and lower tier plank

All vehicle pick ups disabled

Biome deco spawning tweaks

ReAdded Menu Music

Added Stay Clear Modlet

Updated Spherecore

Updated Caves

Removed Traders from Prefabs
